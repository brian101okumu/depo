package com.pizzapap;

import io.vertx.core.AbstractVerticle;

public class FoodAppService extends AbstractVerticle {

    @Override
    public void start() {
    Router r=Router.router(this.vertx);
    r.get("/news").handler(This::news);
    this.vertx.createhttpServer ()
    .requestHandler(r::accept)
    .listen(8081);
    }
    private void news(RoutingContext rc){
        rc.response(.end("Brian Churchill Okumu");
    }
}
